A responsive bootstrap website created by following [this tutorial](https://www.youtube.com/watch?v=9cKsq14Kfsw).

[Check out the website](https://ermy.gitlab.io/bootstrap-website-example)
